<?php


namespace App\Contract\Validator;

interface RuleInterface
{
    public function check($value): bool;
}
