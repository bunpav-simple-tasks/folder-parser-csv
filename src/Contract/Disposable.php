<?php

namespace App\Contract;

interface Disposable
{
    public function dispose(): void;
}
