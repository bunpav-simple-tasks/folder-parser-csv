<?php


namespace App\Contract;

interface CsvReaderInterface
{
    public function open(string $path): self;

    public function read(): \Iterator;
}
