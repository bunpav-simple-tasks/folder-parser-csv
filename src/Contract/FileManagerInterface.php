<?php
namespace App\Contract;

interface FileManagerInterface
{
    /**
     * @param string $path
     * @return bool
     */
    public function exists(string $path): bool;

    /**
     * @param string $path
     * @return bool
     */
    public function create(string $path): bool;

    /**
     * @param string $path
     * @return string|float
     */
    public function get(string $path);

    /**
     * @param string $path
     * @param string|float $value
     * @return bool
     */
    public function set(string $path, $value) :bool;

    public function append(string $path, $value): bool;
}
