<?php


namespace App\Service;

use App\Contract\CsvReaderInterface;
use App\Contract\FileManagerInterface;
use App\Service\Validator\TextValidator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\SplFileInfo;

class StorageService
{
    /** @var FileManagerInterface */
    private $fileManager;

    /** @var Filesystem  */
    private $fileSystem;

    /** @var CsvReaderInterface */
    private $csvReader;

    /** @var TextValidator */
    private $textValidator;

    /** @var string  */
    private $analizeFolder;
    private $dateIndexFolder;
    private $outputFile;


    private $columns;

    /**
     * StorageService constructor.
     * @param FileManagerInterface $fileManager
     * @param Filesystem $fileSystem
     * @param CsvReaderInterface $csvReader
     * @param TextValidator $textValidator
     * @param string $analizeFolder
     */
    public function __construct(FileManagerInterface $fileManager, Filesystem $fileSystem, CsvReaderInterface $csvReader, TextValidator $textValidator, string $analizeFolder)
    {
        $this->fileManager = $fileManager;
        $this->fileSystem = $fileSystem;
        $this->csvReader = $csvReader;
        $this->textValidator = $textValidator;

        $this->analizeFolder = $analizeFolder;
        $this->dateIndexFolder = $analizeFolder.'date/';
        $this->outputFile = $analizeFolder.'output.csv';

        $this->columns = [];
    }

    /**
     * @return string
     */
    public function getAnalizeFolder(): string
    {
        return $this->analizeFolder;
    }

    /**
     * @return string
     */
    public function getDateIndexFolder(): string
    {
        return $this->dateIndexFolder;
    }

    public function buildIndex(SplFileInfo $file): void
    {
        $iterator =  $this->csvReader->open($file)->read();
        $line = $iterator->current();
        $columns = str_getcsv($line, ';');
        $iterator->next();

        while ($iterator->valid()) {
            $line = $iterator->current();
            if (empty($line)) {
                break;
            }
            $this->lineIndexParse($columns, $line);
            $iterator->next();
        }
    }

    public function createOutput(\IteratorAggregate $indexFileIterator)
    {
        $columns = $this->getColumns();
        $this->fileManager->create($this->outputFile);
        $this->fileManager->append($this->outputFile, implode(';', array_merge(['date'], $columns)).\PHP_EOL);

        /** @var SplFileInfo $indexInfo */
        foreach ($indexFileIterator as $indexInfo) {
            $date = $indexInfo->getFileName();
            $records = [$date];
            foreach ($columns as $column) {
                $indexFile = $this->analizeFolder.$column.'/'.$date;
                $records[] = $this->fileManager->exists($indexFile)
                    ? ' '. $this->fileManager->get($indexFile)
                    : ' '. 0;
            }
            $this->fileManager->append($this->outputFile, implode(';', $records).\PHP_EOL);
        }

        $this->fileManager->dispose();
    }

    private function getColumns()
    {
        return array_keys($this->columns);
    }



    private function lineIndexParse(array $columns, string $line)
    {
        $cells = str_getcsv($line, ';');
        if (count($cells) !== count($columns)) {
            return;
        }
        $parsed = array_combine($columns, $cells);
        $date = array_shift($parsed);
        $dateIndex = $this->dateIndexFolder.$date;

        if ($this->textValidator->isValid($date) === false) {
            return;
        }

        $this->saveIndex($dateIndex, '', true);

        foreach ($parsed as $column => $value) {
            $this->columns[$column] = 1;
            $value = is_numeric($value) ? floatval($value) : 0 ;
            $indexFile = $this->analizeFolder.$column.'/'.$date;

            if (!$this->fileSystem->exists($this->analizeFolder.$column)) {
                $this->fileSystem->mkdir($this->analizeFolder.$column);
            }

            $saved = $this->saveIndex($indexFile, $value, true);

            if ($saved === false) {
                $prependValue = floatval($this->fileManager->get($indexFile));
                $this->saveIndex($indexFile, ($value + $prependValue));
            }
        }
    }

    private function saveIndex($file, $value, bool $once = false): bool
    {
        $result = false;

        if (!$this->fileManager->exists($file)) {
            $result = $this->fileManager->create($file);
            $result = $result && $this->fileManager->set($file, $value);
        } elseif ($once === false) {
            $result = $this->fileManager->set($file, $value);
        }

        return $result;
    }
}
