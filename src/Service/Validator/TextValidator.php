<?php

namespace App\Service\Validator;

use App\Contract\Validator\RuleInterface;

class TextValidator
{
    /** @var RuleInterface */
    private $rule;

    /**
     * TextValidator constructor.
     * @param RuleInterface $rule
     */
    public function __construct(RuleInterface $rule)
    {
        $this->rule = $rule;
    }

    /**
     * @param RuleInterface $rule
     * @return TextValidator
     */
    public function setRule(RuleInterface $rule): TextValidator
    {
        $this->rule = $rule;
        return $this;
    }



    public function isValid($value): bool
    {
        return $this->rule->check($value);
    }
}
