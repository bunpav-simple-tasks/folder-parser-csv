<?php


namespace App\Service\Validator\Rule;

use App\Contract\Validator\RuleInterface;

class DateRule implements RuleInterface
{
    public function check($value): bool
    {
        try {
            $check = new \DateTimeImmutable($value);
        } catch (\Throwable $error) {
            return false;
        }

        return $check->format('Y-m-d') === $value;
    }
}
