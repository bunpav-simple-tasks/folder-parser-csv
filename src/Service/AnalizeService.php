<?php
namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class AnalizeService
{

    /** @var Finder */
    private $fileFinder;

    /** @var Filesystem  */
    private $fileSystem;

    /** @var StorageService */
    private $storageService;

    /**
     * AnalizeService constructor.
     * @param Finder $fileFinder
     * @param Filesystem $fileSystem
     * @param StorageService $storageService
     */
    public function __construct(Finder $fileFinder, Filesystem $fileSystem, StorageService $storageService)
    {
        $this->fileFinder = $fileFinder;
        $this->fileSystem = $fileSystem;
        $this->storageService = $storageService;
    }


    private function preparedSystem(): void
    {
        if ($this->fileSystem->exists($this->storageService->getAnalizeFolder())) {
            $this->fileSystem->remove($this->storageService->getAnalizeFolder());
        }

        $this->fileSystem->mkdir($this->storageService->getDateIndexFolder());
    }

    public function analyze(string $path): bool
    {
        $this->preparedSystem();

        if (!$this->fileSystem->exists($path)) {
            return false;
        }

        $csvFinder = $this->fileFinder->create()
            ->files()
            ->name('*.csv')
            ->in($path)
        ;

        /** @var SplFileInfo $file */
        foreach ($csvFinder as $file) {
            $this->storageService->buildIndex($file);
        }

        $indexFilesFinder = $this->fileFinder->create()
            ->files()
            ->in($this->storageService->getDateIndexFolder())
            ->sortByName();

        $this->storageService->createOutput($indexFilesFinder);

        return true;
    }
}
