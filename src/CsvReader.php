<?php


namespace App;

use App\Contract\CsvReaderInterface;
use App\Contract\Disposable;

class CsvReader implements CsvReaderInterface, Disposable
{
    private $file;

    public function open(string $path): CsvReaderInterface
    {
        $this->file = fopen($path, 'r');
        return $this;
    }

    public function read(): \Iterator
    {
        $line = '';
        while ($line !== false) {
            $line = fgets($this->file);
            yield trim($line);
        }

        $this->dispose();
    }

    public function dispose(): void
    {
        fclose($this->file);
    }
}
