<?php

namespace App\Command;

use App\Service\AnalizeService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseFolderCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'analyze:folder';
    /** @var AnalizeService */
    private $analizeSystem;
    private $outputFile;

    protected function configure()
    {
        $this
            ->setDescription('Анализирует папку')
            ->setHelp('Команда сканирует указанную папку и ищет csv файл заданного образца, для составления отчёта')
            ->addArgument('path', InputArgument::REQUIRED, 'полный путь к сканируемой папке')
        ;
    }

    public function __construct(AnalizeService $analizeSystem, string $name = null)
    {
        parent::__construct($name);

        $this->analizeSystem = $analizeSystem;
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $startFolder = $input->getArgument('path');

        $output->writeLn([
            'Аналитика',
            '=========',
            'Парсим каталог: ' . $startFolder,
            ''
        ]);

        $this->analizeSystem->analyze($startFolder);

        $output->writeln(["Сканирование завершено"]);

        return 0;
    }
}
