<?php

namespace App;

use App\Contract\Disposable;
use App\Contract\FileManagerInterface;

class FileManager implements FileManagerInterface, Disposable
{
    private $files;

    public function __construct()
    {
        $this->files = [];
    }

    public function exists(string $path): bool
    {
        return !empty($this->files[$path]);
    }

    public function create(string $path): bool
    {
        $this->files[$path] = [
            'resource' => fopen($path, 'w+'),
            'size' => 0
        ];

        return $this->files[$path]['resource'] !== false;
    }

    public function get(string $path)
    {
        fseek($this->files[$path]['resource'], 0);

        return fread($this->files[$path]['resource'], $this->files[$path]['size']);
    }

    public function set(string $path, $value): bool
    {
        fseek($this->files[$path]['resource'], 0);
        $this->files[$path]['size'] = fwrite($this->files[$path]['resource'], $value);

        return $this->files[$path]['size'] > 0;
    }

    public function append(string $path, $value): bool
    {
        $this->files[$path]['size'] = fwrite($this->files[$path]['resource'], $value);

        return $this->files[$path]['size'] > 0;
    }


    public function dispose(): void
    {
        foreach ($this->files as $resource) {
            if (is_resource($resource['resource']) === true) {
                fclose($resource['resource']);
            }
        }
    }
}
