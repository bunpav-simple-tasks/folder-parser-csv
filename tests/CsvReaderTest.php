<?php

namespace Test;

use App\Contract\CsvReaderInterface;
use App\CsvReader;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

class CsvReaderTest extends TestCase
{
    private $fileSystem;

    protected function setUp(): void
    {
        parent::setUp();

        $directory = [
            'output.csv' => "a;b;c\n1;2;3"
        ];

        $this->fileSystem = vfsStream::setup('root', 444, $directory);
    }


    public function testOpen()
    {
        $reader = new CsvReader();
        $instance = $reader->open($this->fileSystem->url().'/output.csv');
        $this->assertInstanceOf(CsvReaderInterface::class,$instance);

    }

    public function testRead()
    {
        $reader = new CsvReader();
        $iterator = $reader->open($this->fileSystem->url().'/output.csv')->read();
        $line = $iterator->current();
        $this->assertStringContainsString($line,"a;b;c");
        $this->assertTrue($iterator->valid());
        $iterator->next();
        $line = $iterator->current();
        $this->assertStringContainsString($line,'1;2;3');
        $this->assertTrue($iterator->valid());
        $iterator->next();
        $this->assertEmpty($iterator->current());


    }
}
