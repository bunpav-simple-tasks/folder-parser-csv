<?php

namespace Test\Service\Validator\Rule;

use App\Service\Validator\Rule\DateRule;
use PHPUnit\Framework\TestCase;

class DateRuleTest extends TestCase
{

    /**
     * @dataProvider validDateValues
     */
    public function testCheck(string $date)
    {
        $rule = new DateRule();
        $this->assertTrue($rule->check($date));
    }

    /**
     * @dataProvider unValidDateValues
     */
    public function testCheckUnValid(string $date)
    {
        $rule = new DateRule();
        $this->assertFalse($rule->check($date),'Не должно быть правильно');
    }

    public function validDateValues(): array
    {
        return [
            ['2018-10-01'],
            ['2018-10-31'],
        ];
    }

    public function unValidDateValues(): array
    {
        return [
            ['some-string'],
            ['2018-10-01s'],
            ['2018-10-32'],
            ['2018-13-12'],
        ];
    }
}
