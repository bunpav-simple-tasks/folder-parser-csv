<?php

namespace Test\Service\Validator;

use App\Service\Validator\Rule\DateRule;
use App\Service\Validator\TextValidator;
use PHPUnit\Framework\TestCase;

class TextValidatorTest extends TestCase
{

    public function testSetRule()
    {
        $validator = new TextValidator(new DateRule());
        $this->assertInstanceOf(TextValidator::class,$validator);

    }

    /**
     * @dataProvider validDateValues
     * @param string $date
     */
    public function testIsValid(string $date)
    {
        $validator = new TextValidator(new DateRule());
        $this->assertTrue($validator->isValid($date));
    }


    /**
     * @dataProvider unValidDateValues
     * @param string $date
     */
    public function testCheckUnValid(string $date)
    {
        $validator = new TextValidator(new DateRule());
        $this->assertFalse($validator->isValid($date),'Не должно быть правильно');
    }

    public function validDateValues(): array
    {
        return [
            ['2018-10-01'],
            ['2018-10-31'],
        ];
    }

    public function unValidDateValues(): array
    {
        return [
            ['some-string'],
            ['2018-10-01s'],
            ['2018-10-32'],
            ['2018-13-12'],
        ];
    }
}
