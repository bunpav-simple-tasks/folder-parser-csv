<?php

namespace Test\Service;

use App\CsvReader;
use App\FileManager;
use App\Service\AnalizeService;
use App\Service\StorageService;
use App\Service\Validator\Rule\DateRule;
use App\Service\Validator\TextValidator;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class AnalizeServiceTest extends TestCase
{

    private $fileSystem;
    private $analizeFolder;
    private $dateFolder;


    private $storage;

    protected function setUp(): void
    {
        parent::setUp();

        $directory = [
            'test-data'=>[
                'folder'=> [
                    'file.csv' => "date; a; b; c\n2018-01-01; 1; 2; 3\n2018-0103;1 ;3; 2\n2018-01-02;1;2;3",
                    'sub-folder'=> [
                        'sub-file.csv' => "date; a; b; c\n2018-01-01; 1; 2; 3\n2018-01-03;1s ; 3;2\n2018-01-02;1;2;3"
                    ]
                ]
            ],
            'analize-test'=> [
                'analize'=>[],
                'date'=>[]
            ]
        ];

        $this->fileSystem = vfsStream::setup('root', 444, $directory);

        $this->analizeFolder = $this->fileSystem->url().'/analize-test/';
        $this->dateFolder = $this->analizeFolder.'date/';

        $this->storage = new StorageService(new FileManager(),new Filesystem() ,new CsvReader(),new TextValidator(new DateRule()),$this->fileSystem->url().'/analize-test/');
    }

    public function testAnalyze()
    {
        $analize = new AnalizeService(new Finder(), new Filesystem(), $this->storage);
        $this->assertTrue($analize->analyze($this->fileSystem->url().'/test-data/'));

        $needleTxt = <<<NEEDLE
date; a; b; c
2018-01-01; 2; 4; 6
2018-01-02; 2; 4; 6
2018-01-03; 0; 3; 2

NEEDLE;
        $fileText = file_get_contents($this->analizeFolder.'output.csv');
        $this->assertStringContainsString($fileText, $needleTxt);
    }
}
