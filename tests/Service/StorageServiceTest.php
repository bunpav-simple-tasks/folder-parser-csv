<?php

namespace Test\Service;

use App\CsvReader;
use App\FileManager;
use App\Service\StorageService;
use App\Service\Validator\Rule\DateRule;
use App\Service\Validator\TextValidator;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class StorageServiceTest extends TestCase
{
    private $fileSystem;
    private $analizeFolder;
    private $dateFolder;


    private $storage;

    protected function setUp(): void
    {
        parent::setUp();

        $directory = [
            'test-data'=>[
                'folder'=> [
                    'file.csv' => "date; a; b; c\n2018-01-01; 1; 2; 3\n2018-0103;1 ;3; 2\n2018-01-02;1;2;3",
                    'sub-folder'=> [
                        'sub-file.csv' => "date; a; b; c\n2018-01-01; 1; 2; 3\n2018-01-03;1s ; 3;2\n2018-01-02;1;2;3"
                    ]
                ]
            ],
            'analize-test'=> [
                'analize'=>[],
                'date'=>[]
            ]
        ];

        $this->fileSystem = vfsStream::setup('root', 444, $directory);

        $this->analizeFolder = $this->fileSystem->url().'/analize-test/';
        $this->dateFolder = $this->analizeFolder.'date/';

        $this->storage = new StorageService(new FileManager(),new Filesystem() ,new CsvReader(),new TextValidator(new DateRule()),$this->fileSystem->url().'/analize-test/');
    }

    public function testGetDateIndexFolder()
    {
        $this->assertStringContainsString($this->storage->getDateIndexFolder(), $this->dateFolder);
    }

    public function testBuildIndex()
    {
        $csvFinder = (new Finder())
                ->files()
                ->name('*.csv')
                ->in($this->fileSystem->url().'/test-data/')
        ;

        /** @var SplFileInfo $file */
        foreach ($csvFinder as $file)
        {
            $this->storage->buildIndex($file);
        }

        $this->assertFileExists($this->dateFolder.'2018-01-01');
        $this->assertFileExists($this->dateFolder.'2018-01-02');
        $this->assertFileNotExists($this->dateFolder.'2018-0103');

        $this->assertDirectoryExists($this->analizeFolder.' a');
        $this->assertDirectoryExists($this->analizeFolder.' b');
        $this->assertDirectoryExists($this->analizeFolder.' c');
    }

    public function testCreateOutput()
    {
        $csvFinder = (new Finder())
            ->files()
            ->name('*.csv')
            ->in($this->fileSystem->url().'/test-data/')
        ;

        /** @var SplFileInfo $file */
        foreach ($csvFinder as $file)
        {
            $this->storage->buildIndex($file);
        }

        $indexFilesFinder = (new Finder())
            ->files()
            ->in($this->storage->getDateIndexFolder())
            ->sortByName();

        $this->storage->createOutput($indexFilesFinder);

        $this->assertFileExists($this->analizeFolder.'output.csv');

        $needleTxt = <<<NEEDLE
date; a; b; c
2018-01-01; 2; 4; 6
2018-01-02; 2; 4; 6
2018-01-03; 0; 3; 2

NEEDLE;
        $fileText = file_get_contents($this->analizeFolder.'output.csv');
        $this->assertStringContainsString($fileText, $needleTxt);
    }

    public function testGetAnalizeFolder()
    {
        $this->assertStringContainsString($this->storage->getAnalizeFolder(), $this->analizeFolder);
    }
}
