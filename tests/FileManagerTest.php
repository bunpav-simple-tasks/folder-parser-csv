<?php

namespace Test;

use App\FileManager;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

class FileManagerTest extends TestCase
{

    private $fileSystem;
    private $testFile;

    protected function setUp(): void
    {
        parent::setUp();

        $directory = [
            'output.csv' => "a;b;c\n1;2;3"
        ];

        $this->fileSystem = vfsStream::setup('root', 444, $directory);
        $this->testFile = $this->fileSystem->url().'/test.file';
    }


    public function testCreate()
    {
        $manager = new FileManager();
        $this->assertTrue($manager->create($this->testFile));
        $this->assertTrue(is_file($this->testFile));
    }

    public function testAppend()
    {
        $manager = new FileManager();
        $this->assertTrue($manager->create($this->testFile));
        $this->assertTrue($manager->set($this->testFile,1));
        $this->assertTrue($manager->append($this->testFile,2));

        $this->assertStringContainsString(file_get_contents($this->testFile), '12');
    }

    public function testGet()
    {
        $manager = new FileManager();
        $this->assertTrue($manager->create($this->testFile));
        $this->assertTrue($manager->set($this->testFile,'some-text'));
        $this->assertStringContainsString(file_get_contents($this->testFile), 'some-text');
    }

    public function testSet()
    {
        $manager = new FileManager();
        $this->assertTrue($manager->create($this->testFile));
        $this->assertTrue($manager->set($this->testFile,'some-text'));
        $this->assertTrue($manager->set($this->testFile,'some-text-new'));
        $this->assertStringContainsString(file_get_contents($this->testFile), 'some-text-new');
    }

    public function testExists()
    {
        $manager = new FileManager();
        $this->assertFalse(is_file($this->testFile));
        $this->assertFalse($manager->exists($this->testFile));
        $this->assertTrue($manager->create($this->testFile));
        $this->assertTrue($manager->exists($this->testFile));
        $this->assertTrue(is_file($this->testFile));
    }
}
