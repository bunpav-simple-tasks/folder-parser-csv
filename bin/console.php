#!/usr/bin/env php
<?php
require __DIR__.'/../vendor/autoload.php';

use App\Command\ParseFolderCommand;
use Symfony\Component\Console\Application;
use App\Contract\CsvReaderInterface;
use App\Contract\FileManagerInterface;
use App\Contract\Validator\RuleInterface;
use App\CsvReader;
use App\FileManager;
use App\Service\AnalizeService;
use App\Service\StorageService;
use App\Service\Validator\Rule\DateRule;
use App\Service\Validator\TextValidator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;


$containerBuilder = new ContainerBuilder();

$containerBuilder->register(FileManagerInterface::class, FileManager::class);
$containerBuilder->register(Finder::class,Finder::class);
$containerBuilder->register(Filesystem::class,Filesystem::class);
$containerBuilder->register(CsvReaderInterface::class,CsvReader::class);
$containerBuilder->register(RuleInterface::class,DateRule::class);

$containerBuilder->register(TextValidator::class,TextValidator::class)
    ->addArgument(new Reference(RuleInterface::class));

$containerBuilder->register(StorageService::class,StorageService::class)
    ->addArgument(new Reference(FileManagerInterface::class))
    ->addArgument(new Reference(Filesystem::class))
    ->addArgument(new Reference(CsvReaderInterface::class))
    ->addArgument(new Reference(TextValidator::class))
    ->addArgument(dirname(__DIR__).'/analize/')
;

$containerBuilder->register(AnalizeService::class, AnalizeService::class)
    ->addArgument(new Reference(Finder::class))
    ->addArgument(new Reference(Filesystem::class))
    ->addArgument(new Reference(StorageService::class))

;

$containerBuilder->register(ParseFolderCommand::class, ParseFolderCommand::class)
    ->addArgument(new Reference(AnalizeService::class))
    ;

$application = new Application();

$application->add($containerBuilder->get(ParseFolderCommand::class));

$application->run();